function mergeSort(arr) {
  var len = arr.length;
  if (len < 2) {
    return arr;
  }

  var mid = Math.floor(len / 2),
    left = arr.slice(0, mid),
    right = arr.slice(mid);

  return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right) {
  var result = [],
    lLen = left.length,
    rLen = right.length,
    l = 0,
    r = 0;

  while (l < lLen && r < rLen) {
    if (left[l] < right[r]) {
      let le = left[l++];

      result.push(le);
    } else {
      let ri = right[r++];

      result.push(ri);
    }
  }

  return result.concat(left.slice(l)).concat(right.slice(r));
}

module.exports = mergeSort;
