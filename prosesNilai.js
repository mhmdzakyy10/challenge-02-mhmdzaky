const InputNilai = require("./input");
const mergeSort = require("./mergesort");

class ProsesNilai extends InputNilai {
  constructor() {
    super();
  }

  insertNilai() {
    console.log("==============  PROGRAM INPUT NILAI   ==============");
    console.log("============== Muhammad Zaky Aonillah ==============\n");
    console.log(`Inputkan Nilai dan ketik 'q' jika sudah selesai:`);
    this.inputArray();
  }

  inputArray() {
    super.readline().question("nilai: ", (answer) => {
      if (answer == "q") {
        console.log("\n ============== AKUMULASI NILAI ==============");
        console.log("Nilai Tertinggi                   : ", this.getMaxValue());
        console.log("Nilai Terendah                    : ", this.getMinValue());
        console.log("Jumlah Mahasiswa Lulus            : ", this.getLulus());
        console.log("Rata - rata nilai                 : ", this.getAvgValue());
        console.log("urutan nilai terendah ke tertiggi : ", mergeSort(super.getArr));
        super.readline().close();
      } else {
        this.setArr = parseInt(answer);
        this.inputArray();
      }
    });
  }

  getMaxValue() {
    let temp = mergeSort(super.getArr);
    return temp[temp.length - 1];
  }

  getMinValue() {
    let temp = mergeSort(super.getArr);
    return temp[0];
  }

  getAvgValue() {
    let temp = super.getArr;
    let res = 0;
    temp.forEach((val) => {
      res += val;
    });

    return res / temp.length;
  }

  getLulus() {
    let temp = super.getArr;
    let lulus = 0;
    let tidakLulus = 0;
    temp.forEach((val) => {
      if (val >= 75) {
        lulus += 1;
      } else if (val < 75) {
        tidakLulus += 1;
      }
    });
    return { lulus, tidakLulus };
  }
}

module.exports = ProsesNilai;
