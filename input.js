const readline = require("readline");

class InputNilai {
  constructor() {
    this.arr = [];
    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    this.rl.on("close", function () {
      console.log("\n ==============    THANK YOU!   ==============");
      process.exit(0);
    });
  }
  readline() {
    return this.rl;
  }

  get getArr() {
    return this.arr;
  }

  set setArr(arr) {
    this.arr.push(arr);
  }
}

module.exports = InputNilai;
